const Chance = require('chance');
const fs = require('fs');

const chance = new Chance();

function saveJSON(amount) {
  for (let i = 0; i <= amount; i++) {
    let item = {
      username: chance.email(),
      role: chance.pickone(['customer', 'admin']),
      securityLevel: chance.pickone(['simple', 'advenced']),
      balance: {
        assets: {
          THBEX: chance.natural({ max: 100 }),
          ETHDEMO: chance.natural({ max: 100 })
        },
        ETH: chance.floating({ min: 0, max: 100, fixed: 2 })
      },
      createdAt: {
        $date: chance.timestamp()
      },
      emails: [{
        address: chance.email(),
        verified: chance.bool()
      }],
      profile: {
        addresses: {
          bitcoin: chance.hash(),
          ethereum: chance.hash()
        },
        currency: chance.currency().code,
        lang: chance.string({ length: 2 }),
        logins: [{
          app: 'apk',
          app_version: '0.8.3532-remit-th',
          user_agent: 'Mozilla/5.0',
          device: 'LENOVO Lenovo A6010'
        }],
        subscribe: false,
      },
      ref: {
        ref_code: 'FgK6KEh'
      },
      reg: {
        created_date: chance.timestamp(),
        ip_address: '223.24.66.26',
        user_agent: 'Mozilla/5.0',
        referer: 'https://wee.google.com',
        ref_code: 'FgK6KEg',
        vid: 'vFe8ASbo',
        wallet_version: '0.8.3524-remit-th',
        app: 'apk',
        app_version: '0.8.3532-remit-th',
        device: 'LENOVO Lenovo A6010',
        account_src: 'email'
      },
      session: {
        last_active: chance.timestamp(),
        ip: chance.ip(),
        user_agent: 'Mozilla/5.0'
      }
    };
    fs.appendFileSync(`${ __dirname }/fake-data.json`, JSON.stringify(item) + '\r\n', (err) => {
      if (err) return console.log(err);
    });
  }
}

saveJSON(200000);
