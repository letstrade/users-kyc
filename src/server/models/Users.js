const mongoose = require('mongoose');

const { Schema } = mongoose;

const createdAtSchema = new Schema({
  $date: Number
});

const emailSchema = new Schema({
  address: String,
  verified: Boolean
});

const loginsSchema = new Schema({
  app: String,
  app_version: String,
  user_agent: String,
  device: String
});

const profileSchema = new Schema({
  addresses: Object,
  currency: String,
  lang: String,
  logins: [loginsSchema],
  subscribe: Boolean
});

const refSchema = new Schema({
  ref_code: String
});

const regSchema = new Schema({
  created_date: Number,
  ip_address: String,
  user_agent: String,
  referer: String,
  ref_code: String,
  vid: String,
  wallet_version: String,
  app: String,
  app_version: String,
  device: String,
  account_src: String
});

const sessionSchema = new Schema({
  last_active: Number,
  ip: String,
  user_agent: String
});

const userSchema = new Schema({
  username: String,
  role: String,
  securityLevel: String,
  balance: Object,
  createdAt: createdAtSchema,
  emails: [emailSchema],
  profile: profileSchema,
  ref: refSchema,
  reg: regSchema,
  session: sessionSchema
});

module.exports = mongoose.model('users', userSchema);
