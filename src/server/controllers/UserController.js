const BaseController = require('./BaseController');
const Users = require('../models/Users');
const MongoQS = require('mongo-querystring');
const regexpEsc = require('escape-string-regexp');

const MAX_PAGE_SIZE = 100;
const DEFAULT_PAGE_SIZE = 10;
const mongoQS = new MongoQS();

class UserController extends BaseController {

  async getById({ id }) {
    const paramsValidationSchema = {
      id: this.validator.objectId()
    };
    const request = await this.validator.validate({ id }, paramsValidationSchema);
    try {
      const user = await Users.findById(request.id);
      if (!user) {
        return this.res.status(404).send();
      }
      this.res.send(user);
    } catch (error) {
      console.log(error);
      this.res.send(error.toString());
    }
  }

  async getList() {
    try {
      const paramsValidationSchema = {
        filter: this.validator.object().allow({}), // check it later
        customFilters: this.validator.object().allow({}), // check it later
        page: this.validator.number().min(0),
        pageSize: this.validator.number().positive().max(MAX_PAGE_SIZE),
        sort: this.validator.string().max(100).allow('')
      };

      const validQueryData = await this.validator.validate(this.req.query, paramsValidationSchema);
      const page = validQueryData.page;
      const pageSize = validQueryData.pageSize || DEFAULT_PAGE_SIZE;
      const sort = validQueryData.sort || '';

      const filter = this.req.query.filter || {};

      const filterValidator = this.validator.object().keys({
        'reg.created_date': this.validator.date(),
        username: this.validator.string().regex(/^[!^><=$~]{0,2}.*$/),
        'emails.0.verified': this.validator.string().regex(/^[!^><=$~]{0,2}(true|false)$/),
        securityLevel: this.validator.string(),
        'balance.assets.THBEX': this.validator.string().regex(/^[!^><=$~]{0,2}\d+$/),
        'session.last_active': this.validator.date(),
        'profile.lang': this.validator.string().regex(/^[!^><=$~]{0,2}[A-Z]{0,2}$/i),
        'session.ip': this.validator.string().regex(/^[!^><=$~]{0,2}\d+$/)
      });

      const customFilters = this.req.query.customFilters || {};

      const customFiltersValidator = this.validator.object().keys({
        search: this.validator.string().allow(''),
        thbex: this.validator.boolean()
      });

      try {
        const mongoFilter = { $and: [mongoQS.parse(await filterValidator.validate(filter))] };

        // Apply custom filters
        const customMongoFilter = await customFiltersValidator.validate(customFilters);
        if (customMongoFilter.search) {
          mongoFilter.$and.push({
            $or: [
              { username: { $regex: regexpEsc(customMongoFilter.search) } },
              { 'profile.addresses.ethereum': { $regex: regexpEsc(customMongoFilter.search) } },
              { 'reg.refcode': { $regex: regexpEsc(customMongoFilter.search) } },
              { 'reg.referer': { $regex: regexpEsc(customMongoFilter.search) } },
              { 'ref.ref_code': { $regex: regexpEsc(customMongoFilter.search) } }
            ]
          });
        }

        if (customMongoFilter.thbex) {
          mongoFilter.$and.push({ 'balance.assets.THBEX': { $gt: 0 } });
        }

        // hide all private data
        mongoFilter.$and.push({ access: { $ne: 'private' } });

        // skip for pagination
        const skip = page * pageSize;
        this.res.send({
          rows: await Users.find(mongoFilter).skip(skip).limit(pageSize).sort(sort),
          pages: Math.ceil(await Users.count(mongoFilter) / pageSize)
        });
      } catch (error) {
        console.log(error.toString());
        this.res.send(error);
      }
    } catch (error) {
      console.log(error.toString());
      this.res.send(error);
    }
  }

  async stats() {
    try {
      const total = await Users.count();
      const verified = await Users.count({ 'emails.0.verified': true });
      const waitingVerification = await Users.count({ 'emails.0.verified': false });
      this.res.send({
        total,
        verified,
        waiting_verification: waitingVerification
      });
    } catch (error) {
      console.log(error.toString());
      this.res.status(500).send();
    }
  }
}

module.exports = UserController;
