const MongoQS = require('mongo-querystring');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const mongoqsParser = new MongoQS();

class BaseController {
  constructor(req, res) {
    this.req = req;
    this.res = res;
  }

  get validator() {
    return Joi;
  }
}

module.exports = BaseController;
