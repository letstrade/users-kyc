require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = process.env.PORT || 8080;

// Routes
const routes = require('./routes');

app.use(express.static('dist'));
routes(app);

// Bootstrap models
require('./models/Users');

// Connect to mongo
if (!process.env.DB_URL) {
  console.log('Please set up env variable DB_NAME');
  process.exit();
}

function connect() {
  mongoose.connect(process.env.DB_URL);
  return mongoose.connection;
}

function listen() {
  app.listen(port, () => console.log(`Listening on port ${port}!`));
}

connect()
  .on('error', err => console.log(err.toString()))
  .on('disconnected', () => setTimeout(connect, 2000)) // every 2 seconds we will try connect to db
  .once('open', listen);
