const UserController = require('./controllers/UserController');

module.exports = (app) => {
  app.get('/api/users/stats', (req, res) => new UserController(req, res).stats());
  app.get('/api/users/:id', (req, res) => new UserController(req, res).getById(req.params));
  app.get('/api/users', (req, res) => new UserController(req, res).getList());
};
