import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.css';

import usersColumns from '../settings/users';
import Table from '../components/Table';
import UsersStats from '../components/UsersStats';
import UsersFilters from '../components/UsersFilters';
import '../assets/css/app.css';

import moment from 'moment';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { filters: {} };

    this.setCustomFilters = this.setCustomFilters.bind(this);
    this.getCustomFilters = this.getCustomFilters.bind(this);
  }

  prepareTd(state, rowInfo) {
    const row = rowInfo && rowInfo.row;
    if (row && row.session) {
      let style;
      switch (true) {
        case moment(row.session.last_active).add(24, 'hour').isAfter(moment()):
        style = {
          color: '#000',
          background: '#008000'
        };
        break;

        case moment(row.session.last_active).add(48, 'hour').isAfter(moment()):
        style = {
          color: '#000',
          background: '#90EE90'
        };
        break;

        case moment(row.session.last_active).add(7, 'day').isAfter(moment()):
        style = {
          color: '#000',
          background: '#d2721c'
        };
        break;

        case moment(row.session.last_active).add(7, 'day').isBefore(moment()):
        style = {
          color: '#fff',
          background: '#000'
        };
        break;
      }
      return {
        style
      };
    }
    return {};
  }

  prepareTr(state, rowInfo, column) {
    const row = rowInfo && rowInfo.row;
    if (row && row['emails.0.verified'] === 'No') {
      return {
        style: {
          color: '#000',
          background: '#ccc'
        }
      };
    }
    return {};
  }

  getCustomFilters() {
    return this.state.filters;
  }

  setCustomFilters(filters) {
    this.setState({ filters });
  }

  render() {
    return (
      <Container fluid>
        <Row>
          <Col>
            <br />
            <h2>Users Admin</h2>
            <UsersStats />
            <UsersFilters handleCustomFilters={this.setCustomFilters} />
            <Row>
              <Col>
                <Table
                  url="/api/users"
                  columns={usersColumns}
                  getCustomFilters={this.getCustomFilters}
                  getTdProps={this.prepareTd}
                  getTrProps={this.prepareTr}
                />

              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}
