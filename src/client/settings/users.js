import moment from 'moment';
import React from 'react';

function formatDate(date) {
  return moment(date).format('YYYY/MM/DD HH:mm:ss');
}

export default [
  {
    Header: 'Created Date',
    id: 'reg.created_date',
    accessor: d => formatDate(d.reg.created_date),
    Filter: ({ filter, onChange }) => ' '
  },
  {
    Header: 'Username',
    accessor: 'username'
  },
  {
    Header: 'Verified',
    id: 'emails.0.verified',
    accessor: d => (d.emails[0].verified ? 'Yes' : 'No'),
    Filter: ({ filter, onChange }) => {
      return <select
        onChange={event => onChange(event.target.value)}
        style={{ width: '100%' }}
        value={filter ? filter.value : 'all'}
      >
        <option value="all">Show All</option>
        <option value="true">Verified</option>
        <option value="false">Wait for verification</option>
      </select>
    }
  },
  {
    Header: 'Security Level',
    id: 'securityLevel',
    accessor: d => `[${d.securityLevel}]`,
    Filter: ({ filter, onChange }) => {
      return <select
        onChange={event => onChange(event.target.value)}
        style={{ width: '100%' }}
        value={filter ? filter.value : 'all'}
      >
        <option value="all">Show All</option>
        <option value="simple">Simple</option>
        <option value="advenced">Advenced</option>
      </select>
    }
  },
  {
    Header: 'THBEX',
    id: 'balance.assets.THBEX',
    accessor: d => d.balance.assets.THBEX
  },
  {
    Header: 'Last active',
    id: 'session.last_active',
    accessor: d => formatDate(d.session.last_active),
    Filter: ({ filter, onChange }) => ' '
  },
  {
    Header: 'Language',
    id: 'profile.lang',
    accessor: d => d.profile.lang
  },
  {
    Header: 'IP',
    id: 'session.ip',
    accessor: d => d.session.ip
  }
];
