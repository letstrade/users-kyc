import React, { Component } from 'react';
import PropTypes from 'prop-types';
import qs from 'qs';

import ReactTable from 'react-table';
import 'react-table/react-table.css';
import '../assets/css/components/table.css';

class Table extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      customFilters: {},
      loading: true,
      pages: 0,
      data: []
    };

    this.fetchData = this.fetchData.bind(this);
  }

  prepareQuery(state) {
    const query = { pageSize: state.pageSize, page: state.page };
    query.sort = '';
    if (state.sorted && Array.isArray(state.sorted)) {
      state.sorted.forEach((sort) => {
        query.sort += `${sort.desc ? '-' : ''}${sort.id}`;
      });
    }

    // prepare filter
    query.filter = {};
    if (state.filtered && Array.isArray(state.filtered)) {
      state.filtered.forEach((field) => {
        query.filter[field.id] = field.value;
      });
    }

    return query;
  }

  fetchData(state) {
    const customFilters = this.props.getCustomFilters();
    const query = this.prepareQuery(state);
    fetch(`${this.props.url}/?${qs.stringify({ filter: query.filter, customFilters })}&sort=${query.sort}&page=${state.page}&pageSize=${state.pageSize}`)
      .then(response => response.json())
      .then((body) => {
        this.setState({
          loading: false,
          pages: body.pages,
          data: body.rows
        });
      })
      .catch((error) => {
        alert('Cannot send request to server.');
        console.log(error.toString());
      });
  }

  componentWillReceiveProps() {
    this.tableInstance.fireFetchData();
  }

  render() {
    const component = this;
    return (
      <ReactTable
        data={this.state.data}
        columns={this.props.columns}
        pages={this.state.pages}
        defaultPageSize={10}
        manual
        filterable
        loading={this.state.loading}
        getTrProps={this.props.getTrProps}
        getTdProps={this.props.getTdProps}
        onFetchData={this.fetchData}
        className="component-table -striped -highlight"
      >
        {(state, makeTable, instance) => {
          component.tableInstance = instance;

          return (
            <div>
              {makeTable()}
            </div>
          );
          }
        }
      </ReactTable>
    );
  }
}

Table.defaultProps = {
  getCustomFilters: () => {}
};

Table.propTypes = {
  url: PropTypes.string.isRequired,
  getCustomFilters: PropTypes.func
};


export default Table;
