import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Row, Col, Card,
  CardBody, CardTitle, Form,
  FormGroup, Label, Input
} from 'reactstrap';

class UsersFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      thbex: false
    };

    this.handleSearchTyping = this.handleSearchTyping.bind(this);
    this.handleTHBEXFilter = this.handleTHBEXFilter.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSearchTyping(event) {
    this.setState({ search: event.target.value }, () => this.props.handleCustomFilters(this.state));
  }

  handleTHBEXFilter(event) {
    this.setState({ thbex: event.target.checked }, () => this.props.handleCustomFilters(this.state));
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    return (
      <Row className="info-block">
        <Col xs="12">
          <Card>
            <CardBody>
              <CardTitle>Filters</CardTitle>
              <Form onSubmit={this.handleSubmit}>
                <FormGroup>
                  <Input type="text" value={this.state.search} onChange={this.handleSearchTyping} placeholder="Search..." />
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input type="checkbox" value={this.state.thbex} onChange={this.handleTHBEXFilter} />{' '}Only with THBEX
                  </Label>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

UsersFilters.propTypes = {
  handleCustomFilters: PropTypes.func.isRequired
};

export default UsersFilters;
