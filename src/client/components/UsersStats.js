import React, { Component } from 'react';
import { Row, Col, Card, CardText, CardBody } from 'reactstrap';

class UsersStats extends Component {
  constructor() {
    super();
    this.state = {
      total: 0,
      verified: 0,
      waiting_verification: 0
    };
    this.updateUsersInfo();
  }

  updateUsersInfo() {
    fetch('/api/users/stats')
      .then(response => response.json())
      .then((body) => {
        this.setState(body);
      })
      .catch((error) => {
        alert('Cannot get users stats from server.');
        console.log(error.toString());
      });
  }

  render() {
    return (
      <Row className="info-block">
        <Col xs="12" md="4">
          <Card>
            <CardBody>
              <CardText>{this.state.total} users total</CardText>
            </CardBody>
          </Card>
        </Col>
        <Col xs="12" md="4">
          <Card>
            <CardBody>
              <CardText>{this.state.verified} active verified users</CardText>
            </CardBody>
          </Card>
        </Col>
        <Col xs="12" md="4">
          <Card>
            <CardBody>
              <CardText>{this.state.waiting_verification} waiting for email verification</CardText>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default UsersStats;
